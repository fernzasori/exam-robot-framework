*** Settings ***
Test Setup        Log To Console    <<<START>>>
Test Teardown     Log To Console    ------------------------------------------------------------------------------
Resource          ../KEYWORDS/Keywords.txt

*** Test Cases ***
WS2_ROBOT
    @{ListData}    [ER] Get Data Row For Excel    TESTDATA/DataTest.xlsx    DATA1    2
    [W] Open Browser    https://demo.abantecart.com/    chrome
    [W] Click Element    //a[contains(.,'Login or register')]
    [W] Input Text    //input[@id='loginFrm_loginname']    @{ListData}[0]
    [W] Input Text    //input[@name='password']    @{ListData}[1]
    [W] Click Element    //button[contains(.,'Login')]
    [W] Click Element    (//a[contains(.,'Makeup')])[2]
    [W] Click Element    (//a[contains(.,'Lips')])[2]
    [W] Click Element    (//a[@class='prdocutname'])[3]
    [W] Click Element    //a[@class='cart']
    [W] Click Element    //select[@name='shippings']//option[2]
    [W] Click Element    (//i[@class='fa fa-shopping-cart fa-fw'])[5]
    [W] Click Element    (//div[@class='checkbox_place'])[2]
    [W] Click Element    //button[contains(.,'Confirm Order')]
    [W] Check Meaasge    Order is completed!
    [W] Click Element    //a[contains(@class,'parent')]
    [W] Click Element    (//a[contains(.,'Account')])[4]
    [W] Click Element    (//a[contains(.,'Logoff')])[2]
    [W] Click Element    //a[@class='btn btn-default mr10']
